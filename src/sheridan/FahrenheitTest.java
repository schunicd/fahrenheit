package sheridan;
//Name: Derek Schunicke
//Student Number: 991295326
import static org.junit.Assert.*;

import org.junit.Test;

public class FahrenheitTest {

	@Test
	public void TestCelciusToFahrenheitConverterRegular() {
		assertTrue("Incorrect Calculation", Fahrenheit.fromCelcius(1) == 34);
	}
	
	@Test
	(expected=IllegalArgumentException.class)
	public void TestCelciusToFahrenheitConverterException() {
		Fahrenheit.fromCelcius(-600);
	}
	
	@Test
	public void TestCelciusToFahrenheitConverterBoundaryIn() {
		assertTrue("Incorrect Calculation", Fahrenheit.fromCelcius(46) == 115);
	}
	
	@Test
	public void TestCelciusToFahrenheitConverterBoundaryOut() {
		assertTrue("Incorrect Calculation", Fahrenheit.fromCelcius(-1) == 30);
	}


}
