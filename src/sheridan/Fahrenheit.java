package sheridan;

public class Fahrenheit {
	
	public static int fromCelcius(int celcius) throws IllegalArgumentException{
		double fahr = 0;
		fahr = ((celcius*9.0)/5.0)+32;	
		if(fahr < -459.67) throw new IllegalArgumentException("TOO COLD!!! WARMER PLEASE!!");
		return (int) Math.round(fahr);
	}

}
